﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraftSetUpPhoto : MonoBehaviour {

    public Material m_left;
    public Material m_right;
    public Transform m_tCenter;
    public Transform m_tLeft;
    public Transform m_tRight;

    public StereoPhoto[] m_photos;
    public int m_index;

    public float m_offset=0.01f;
    
    
    void Start () {
        SetWithIndex(0);
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_index++;
            if (m_index == m_photos.Length)
                m_index = 0;

            SetWithIndex(m_index);

        }
        if (Input.mouseScrollDelta != Vector2.zero)
        {
            m_tLeft.localPosition += Vector3.right * m_offset * Input.mouseScrollDelta.y;
            m_tRight.localPosition += Vector3.left * m_offset * Input.mouseScrollDelta.y;
        }
    }

    private void SetWithIndex(int index)
    {
        m_left.SetTexture("_MainTex", m_photos[index].m_left);
        m_right.SetTexture("_MainTex", m_photos[index].m_right);
        m_tLeft.localPosition = Vector3.left * m_photos[index].m_offset;
        m_tRight.localPosition = Vector3.right * m_photos[index].m_offset;
    }

    [System.Serializable]
    public class StereoPhoto {

        public string m_name;
        public Texture m_left;
        public Texture m_right;
        public float m_offset;
        
    }
}
